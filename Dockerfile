# This image is intended for deploying dukboot.
# E.g. "docker run dukboot/dukboot myscript.js"
#
# Warning: I haven't tested to see if it works yet!!!
# TODO: Properly bundle js/dukboot.js
FROM alpine:latest
RUN apk add --no-cache build-base git && \
    cd /tmp && \
    git clone https://gitlab.com/dukboot/dukboot && \
    cd dukboot && \
    make && \
    mv dukboot /usr/local/bin/ && \
    mv js /usr/local/bin/ && \
    cd /tmp && \
    rm -rf dukboot && \
    apk del --no-cache --purge build-base git
ENTRYPOINT dukboot
