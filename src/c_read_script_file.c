#include <stdio.h>
#include "duktape/duktape.h"
#include "c_read_script_file.h"
int c_read_script_file (const char *joined_path, char **message, char **buffer, size_t *bytesRead) {
  // Open the file
  FILE * pFile = fopen(joined_path, "rb");
  if (pFile == NULL) {
    asprintf(message, "could not open file %s", joined_path);
    return 1;
  }
  // Get file size http://www.cplusplus.com/reference/cstdio/fread/
  fseek(pFile, 0, SEEK_END);
  long int fileSize = ftell(pFile);
  rewind(pFile);
  // Allocate a buffer the write size
  *buffer = (char*) malloc (sizeof(char) * fileSize);
  if (*buffer == NULL) {
    asprintf(message, "could not allocate the %ld bytes of memory to read %s", fileSize, joined_path);
    return 2;
  }
  // Dump file into memory
  *bytesRead = fread(*buffer, 1, fileSize, pFile);
  if (*bytesRead != fileSize) {
    asprintf(message, "failed reading file %s", joined_path);
    return 3;
  }
  // Close the file bc we're done with it
  fclose(pFile);
  return 0;
}
