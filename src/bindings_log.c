#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "duktape/duktape.h"
#include "bindings_log.h"

// For now, copying method names from 'console'.
// In future, support different transports, formats, etc.
void push_bindings_log(duk_context *ctx) {
  duk_push_c_function(ctx, c_log, DUK_VARARGS);
  duk_put_prop_string(ctx, -2, "log");
  duk_push_c_function(ctx, c_log, DUK_VARARGS);
  duk_put_prop_string(ctx, -2, "info");
  duk_push_c_function(ctx, c_log, DUK_VARARGS);
  duk_put_prop_string(ctx, -2, "warn");
  duk_push_c_function(ctx, c_log, DUK_VARARGS);
  duk_put_prop_string(ctx, -2, "error");
}

duk_ret_t c_log(duk_context *ctx) {
  duk_push_string(ctx, " ");
  duk_insert(ctx, 0);
  duk_join(ctx, duk_get_top(ctx) - 1);
  fprintf(stderr, "%s\n", duk_safe_to_string(ctx, -1));
  return 0;
}
