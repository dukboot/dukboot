#include "duktape/duktape.h"
#include "bindings_sandbox.h"

void push_bindings_sandbox(duk_context *ctx) {
  duk_push_c_function(ctx, c_compile_trusted_function, 1);
  duk_put_prop_string(ctx, -2, "compileTrustedFunction");
}

duk_ret_t c_compile_trusted_function (duk_context *ctx) {
  // Safety check filename argument
  app_assert_safe_path(ctx);
  const char *filename = duk_get_string(ctx, -1);
  // Get the fileName property of the calling function.
  duk_inspect_callstack_entry(ctx, -2);
  // int check = duk_has_prop_string(ctx, -1, "function");
  duk_get_prop_string(ctx, -1, "function");
  // check = duk_has_prop_string(ctx, -1, "fileName");
  duk_get_prop_string(ctx, -1, "fileName");
  const char *baseFilename = duk_get_string(ctx, -1);
  char *joined_path = c_join_paths(baseFilename, filename);
  // Replace with absolute filename
  duk_pop(ctx);
  duk_push_string(ctx, joined_path);
  // Read the file
  char *message = NULL;
  char *buffer = NULL;
  size_t bytesRead = 0;
  duk_int_t ret = c_read_script_file(joined_path, &message, &buffer, &bytesRead);
  if (ret) {
    if (buffer != NULL) free(buffer);
    duk_push_error_object(ctx, DUK_ERR_ERROR, "%s", message);
    free(message);
    return duk_throw(ctx);
  }
  // Create a function from the file buffer. Leaves it on the stack.
  duk_compile_lstring_filename(ctx, DUK_COMPILE_FUNCTION + DUK_COMPILE_STRICT, buffer, bytesRead);
  free(buffer);
  if (message != NULL) free(message);
  return 1;
}
