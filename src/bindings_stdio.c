#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "duktape/duktape.h"
#include "bindings_stdio.h"

void push_bindings_stdio(duk_context *ctx) {
  duk_push_c_function(ctx, c_print, 1);
  duk_put_prop_string(ctx, -2, "print");
  duk_push_c_function(ctx, c_getUint8, 0);
  duk_put_prop_string(ctx, -2, "getUint8");
  duk_push_c_function(ctx, c_putUint8, 1);
  duk_put_prop_string(ctx, -2, "putUint8");
  duk_push_c_function(ctx, c_getbuffer, 1);
  duk_put_prop_string(ctx, -2, "getbuffer");
  duk_push_c_function(ctx, c_putbuffer, 1);
  duk_put_prop_string(ctx, -2, "putbuffer");
}

duk_ret_t c_print(duk_context *ctx) {
  fprintf(stdout, duk_safe_to_string(ctx, -1));
  return 0;
}

duk_ret_t c_getUint8 (duk_context *ctx) {
  int c;
  c = fgetc(stdin);
  if (c != EOF) {
    duk_push_int(ctx, c);
    return 1;
  }
  if (feof(stdin)) {
    duk_push_error_object(ctx, errno, "EOF");
    return 1;
  }
  if (ferror(stdin)) {
    if (errno == EWOULDBLOCK) {
      duk_push_undefined(ctx);
      return 1;
    }
    duk_push_error_object(ctx, errno, strerror(errno));
    clearerr(stdin);
    return duk_throw(ctx);
  }
  return duk_error(ctx, 999, "Unhandled situation in c_getUint8.");
}

duk_ret_t c_putUint8 (duk_context *ctx) {
  int c = duk_require_int(ctx, 0);
  if (fputc(c, stdout) != EOF) {
    duk_push_int(ctx, c);
    return 1;
  }
  if (feof(stdout)) {
    duk_push_error_object(ctx, errno, "EOF");
    return 1;
  }
  if (ferror(stdout)) {
    if (errno == EWOULDBLOCK) {
      duk_push_undefined(ctx);
      return 1;
    }
    duk_push_error_object(ctx, errno, strerror(errno));
    clearerr(stdout);
    return duk_throw(ctx);
  }
  return duk_error(ctx, 999, "Unhandled situation in c_putUint8.");
}

duk_ret_t c_getbuffer (duk_context *ctx) {
  char * buffer;
  duk_size_t length;
  buffer = duk_require_buffer_data(ctx, 0, &length);
  size_t bytes_read = fread(buffer, sizeof(char), length, stdin);
  if (bytes_read == 0) {
    if (feof(stdin)) {
      duk_push_error_object(ctx, errno, "EOF");
      return 1;
    }
    if (ferror(stdin)) {
      if (errno == EWOULDBLOCK) {
        duk_push_undefined(ctx);
        return 1;
      }
      duk_push_error_object(ctx, errno, strerror(errno));
      clearerr(stdin);
      return duk_throw(ctx);
    }
    return duk_error(ctx, 999, "Unhandled situation in c_getbuffer.");
  }
  duk_push_int(ctx, bytes_read);
  return 1;
}

duk_ret_t c_putbuffer (duk_context *ctx) {
  char * buffer;
  duk_size_t length;
  buffer = duk_require_buffer_data(ctx, 0, &length);
  size_t written = fwrite(buffer, sizeof(char), length, stdout);
  if (written != length) {
    if (feof(stdout)) {
      duk_push_error_object(ctx, errno, "EOF");
      return 1;
    }
    if (ferror(stdout)) {
      if (errno == EWOULDBLOCK) {
        duk_push_undefined(ctx);
        return 1;
      }
      duk_push_error_object(ctx, errno, strerror(errno));
      clearerr(stdout);
      return duk_throw(ctx);
    }
    return duk_error(ctx, 999, "Unhandled situation in c_putbuffer.");
  }
  duk_push_int(ctx, written);
  return 1;
}
