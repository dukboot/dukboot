#include "duktape/duktape.h"
#include "bindings_main.h"

void push_bindings_main(duk_context *ctx) {
  duk_push_c_function(ctx, c_argv, 0);
  duk_put_prop_string(ctx, -2, "argv");
  duk_push_c_function(ctx, c_environ, 0);
  duk_put_prop_string(ctx, -2, "environ");
}

duk_ret_t c_argv(duk_context *ctx) {
  // Construct argv array
  duk_idx_t argv_idx = duk_push_array(ctx);

  int i = 0;
  while (i < mainArgs.argc && i < 100) {
    duk_push_string(ctx, mainArgs.argv[i]);
    duk_put_prop_index(ctx, argv_idx, i);
    i++;
  }
  return 1;
}

duk_ret_t c_environ(duk_context *ctx) {
  int keylen = 0;
  char *val;
  char *equalsign;

  // Construct environment hash
  duk_idx_t environ_idx = duk_push_object(ctx);

  int i = 0;
  while (environ[i] != NULL && i < 100) {
    // strtok actually replaces the delimiter with \0
    // so I don't want to use it.
    equalsign = strpbrk(environ[i], "=");
    val = equalsign + 1;
    keylen = equalsign - environ[i]; // yay pointer arithmetic
    //printf("%d. %.*s,%s\n", i, keylen, environ[i], val);
    duk_push_string(ctx, val);
    duk_put_prop_lstring(ctx, environ_idx, environ[i], keylen);
    i++;
  }
  return 1;
}
