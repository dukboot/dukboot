#include <time.h>
#include <math.h>
#include <errno.h>
#include <omp.h>
#include "duktape/duktape.h"
#include "bindings_thread.h"
void push_bindings_thread(duk_context *ctx) {
  duk_push_c_function(ctx, c_thread_id, 0);
  duk_put_prop_string(ctx, -2, "id");
  duk_push_c_function(ctx, c_thread_pool, 0);
  duk_put_prop_string(ctx, -2, "pool");
  duk_push_c_function(ctx, c_thread_sleep, 1);
  duk_put_prop_string(ctx, -2, "sleep");
}

duk_ret_t c_thread_id(duk_context *ctx) {
  duk_push_int(ctx, omp_get_thread_num());
  return 1;
}

duk_ret_t c_thread_pool(duk_context *ctx) {
  duk_push_array(ctx);
  size_t numThreads = omp_get_num_threads();
  for (size_t i = 0; i < numThreads; i++) {
    duk_push_int(ctx, i);
    duk_put_prop_index(ctx, -2, i);
  }
  return 1;
}

// Takes its parameter in milliseconds, but
// supports sub-millisecond accuracy by accepting a float.
duk_int_t c_thread_sleep (duk_context *ctx) {
  duk_double_t ms = duk_get_number(ctx, -1);
  duk_double_t sec = 0;
  struct timespec t;
  t.tv_nsec = (long) 1000000000 * modf(ms / 1000.0, &sec);
  t.tv_sec = (time_t) sec;
  int ret = nanosleep(&t, NULL);
  if (ret) {
    switch (errno) {
      case EFAULT: return duk_generic_error(ctx, "System EFAULT");
      break;
      case EINVAL: return duk_range_error(ctx, "System EINVAL (hint: negative arguments to sleep() are invalid)");
      break;
      case EINTR: // TODO: Handle a signal interupt case
      break;
    }
  }
  return 0;
}
