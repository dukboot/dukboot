#include "duktape/duktape.h"
#include "app_unstash_global_string.h"
void app_unstash_global_string(duk_context *ctx, char *name) {
  duk_push_global_stash(ctx); // summon the magic stash object
  // TODO: Throw an error if property not found. (Right now just returns undefined)
  duk_get_prop_string(ctx, -1, name); // retrieve property
  duk_remove(ctx, -2); // Disappear the stash
}
