#include "duktape/duktape.h"
#include <libgen.h>
#include "c_path.h"
char* c_join_paths(const char *base, const char *filename) {
  int base_len = strlen(base);
  int filename_len = strlen(filename);
  char *joined_path = malloc(base_len + filename_len + 5); // 2 for the nulls, 1 for the path separator, 2 more for luck.
  strcpy(joined_path, base);
  dirname(joined_path); // should shorten if anything
  strcat(joined_path, "/");
  strcat(joined_path, filename);
  return joined_path;
}
char* c_dirname(const char *base) {
  int base_len = strlen(base);
  char *base_path = malloc(base_len);
  strcpy(base_path, base);
  dirname(base_path); // should shorten if anything
  return base_path;
}
