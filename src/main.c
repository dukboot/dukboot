// Define externs
extern char **environ;

// Include dependencies
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#ifdef _WIN32
  // @see [1]
  #include <io.h>
  #include <fcntl.h>
#endif
#include <omp.h>
#include "duktape/duktape.h"

#include "main.h"

mainArgs_t mainArgs;
messageBufferA Messages;

int main (int argc, char * argv[] /* char *environ[] */) {
  mainArgs.argc = argc;
  mainArgs.argv = argv;
  // mainArgs.environ = environ;
  // Re-open stdin and stdout in binary mode
  freopen(NULL, "rb", stdin);
  freopen(NULL, "wb", stdout);
  #ifdef _WIN32
    // @see [1]
    _setmode(0, _O_BINARY);
    _setmode(1, _O_BINARY);
  #endif
  // In my experience, output buffering by default is surprising to most
  // beginners. It's premature optimization, and can easily lead to data loss
  // if e.g. writing stdout to a log. If users need buffering they
  // are smart enough to implement that in their app.
  setbuf(stdin, NULL);
  setbuf(stdout, NULL);
  // Finally, async I/O is all the rage for performance these days. Since I fail
  // to see why there should be two ways to do something if one way is better,
  // we'll just try to have a consistant async I/O interface.
  // Note: at the moment this only works on POSIX. :(
  #ifdef UNIX
    int flags = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, flags | O_NONBLOCK);
    flags = fcntl(STDOUT_FILENO, F_GETFL, 0);
    fcntl(STDOUT_FILENO, F_SETFL, flags | O_NONBLOCK);
  #endif

  if (argc < 2) {
    printf("Usage: dukboot file-to-eval.js\n");
    printf("Yup. Pretty basic API at the moment.\n");
    exit(0);
  }

  // Allocate 10 inter-thread communications buffers.
  initExternalBuffers(8+7+6+5+4);

  // Fork into native (gcc) threads.
  duk_int_t result;
  #pragma omp parallel shared(Messages)
  {
    // Create a JS environment
    duk_context *ctx = duk_create_heap_default();
    // Save a copy of the Duktape object...
    app_stash_global_string(ctx, "Duktape");
    // ...then make it inaccessible to JS code.
    duk_push_undefined(ctx);
    duk_put_global_string(ctx, "Duktape");

    // Load top-level module (runtime framework) - relative to argv[0]
    duk_int_t ret = load_runtime_framework(ctx, c_join_paths(c_dirname(argv[0]), "dukboot/js/dukboot.js"));
    if (ret != 0) {
      fprintf(stderr, "%s %s\n", argv[1], duk_safe_to_string(ctx, -1));
      goto finally;
    }

    // Construct the 'dukboot' object
    duk_push_object(ctx);
    // Provide access to the environment
    duk_push_object(ctx);
    push_bindings_main(ctx);
    duk_put_prop_string(ctx, -2, "main");
    // Provide access to stdin and stdout
    duk_push_object(ctx);
    push_bindings_stdio(ctx);
    duk_put_prop_string(ctx, -2, "stdio");
    // Provide access to console.log
    duk_push_object(ctx);
    push_bindings_log(ctx);
    duk_put_prop_string(ctx, -2, "log");
    // Provide access to threading primitives
    duk_push_object(ctx);
    push_bindings_thread(ctx);
    duk_put_prop_string(ctx, -2, "thread");
    // Provide access to TCP sockets
    duk_push_object(ctx);
    push_bindings_netLib(ctx);
    duk_put_prop_string(ctx, -2, "netLib");
    // Provide access to sandbox tools
    duk_push_object(ctx);
    push_bindings_sandbox(ctx);
    duk_put_prop_string(ctx, -2, "sandbox");
    // Provide access to shared memory messages
    duk_push_object(ctx);
    push_bindings_message(ctx);
    duk_put_prop_string(ctx, -2, "message");
    // Provide access to native functions
    app_unstash_global_string(ctx, "Duktape"); // summon from the magic stash
    duk_put_prop_string(ctx, -2, "Duktape");
    duk_push_object(ctx);
    duk_push_c_function(ctx, app_push_dir, 1);
    duk_put_prop_string(ctx, -2, "dir");
    duk_push_c_function(ctx, app_assert_safe_path, 1);
    duk_put_prop_string(ctx, -2, "assert_safe_path");
    duk_put_prop_string(ctx, -2, "modules");
    // Provide direct access to the event loop
    // Create a global (techinically heap-level) event queue
    duk_push_object(ctx);
    queue_create(ctx, "timers");
    queue_push_queue(ctx, "timers");
    duk_put_prop_string(ctx, -2, "timers");
    duk_put_prop_string(ctx, -2, "Queues");

    // Experiment with hiding Uint8Array from code
    // duk_push_undefined(ctx);
    // duk_put_global_string(ctx, "Uint8Array");
    //
    // // Try to prevent ALL buffer creation
    // duk_get_global_string(ctx, "Uint16Array");
    // duk_get_prop_string(ctx, -1, "prototype");
    // duk_get_prop_string(ctx, -1, "BYTES_PER_ELEMENT");
    // fprintf(stderr, "%s\n", duk_safe_to_string(ctx, -1));
    // duk_pop(ctx);

    // duk_get_global_string(ctx, "Object");
    // duk_push_undefined(ctx);
    // duk_put_global_string(ctx, "TypedArray");

    // Instantiate the runtime by running the top-level module, and
    // passing 'dukboot' as the sole argument.
    ret = duk_pcall(ctx, 1);
    if (ret != 0) {
      if (duk_is_error(ctx, -1)) {
        duk_dup(ctx, -1);
        fprintf(stderr, "\033[31m%s\033[39;49m\n",duk_safe_to_string(ctx, -1));
        duk_pop(ctx);
        if (duk_get_prop_string(ctx, -1, "fileName")) {
          fprintf(stderr, "%s", duk_safe_to_string(ctx, -1));
        }
        duk_pop(ctx);
        if (duk_get_prop_string(ctx, -1, "lineNumber")) {
          fprintf(stderr, ":%s\n", duk_safe_to_string(ctx, -1));
        }
        duk_pop(ctx);
      } else {
        duk_push_context_dump(ctx);
        printf("%s\n", duk_to_string(ctx, -1));
      }
      goto finally;
    }

    // Load top-level function (application code) - relative to CWD
    ret = load_main_script(ctx, argv[1]);
    if (ret != 0) {
      fprintf(stderr, "%s %s\n", argv[1], duk_safe_to_string(ctx, -1));
      goto finally;
    }

    // Instantiate the application code by passing it as the
    // sole argument to the runtime code.
    ret = duk_pcall(ctx, 1);
    if (ret != 0) {
      if (duk_is_error(ctx, -1)) {
        duk_dup(ctx, -1);
        fprintf(stderr, "\033[31m%s\033[39;49m\n",duk_safe_to_string(ctx, -1));
        duk_pop(ctx);
        if (duk_get_prop_string(ctx, -1, "fileName")) {
          fprintf(stderr, "%s", duk_safe_to_string(ctx, -1));
        }
        duk_pop(ctx);
        if (duk_get_prop_string(ctx, -1, "lineNumber")) {
          fprintf(stderr, ":%s\n", duk_safe_to_string(ctx, -1));
        }
        duk_pop(ctx);
      } else {
        duk_push_context_dump(ctx);
        printf("%s\n", duk_to_string(ctx, -1));
      }
      goto finally;
    }
    // else {
    //   duk_push_context_dump(ctx);
    //   printf("%s\n", duk_to_string(ctx, -1));
    // }
  finally:
    duk_destroy_heap(ctx);
  #pragma omp critical
    // For now we will OR the exit codes for each thread.
    // That lets you easily check the result against multiple flags.
    result |= ret;
  }
  return result;
}

int load_runtime_framework (duk_context *ctx, const char *filename) {
  char *message = NULL;
  char *buffer = NULL;
  size_t bytesRead = 0;
  duk_int_t ret = c_read_script_file(filename, &message, &buffer, &bytesRead);
  if (ret) {
    if (message != NULL) fprintf(stderr, "%s\n", message);
    if (message != NULL) free(message);
    if (buffer != NULL) free(buffer);
    exit(ret);
  }
  // Create a function from the file buffer. Leaves it on the stack.
  duk_push_string(ctx, filename);
  ret = duk_pcompile_lstring_filename(ctx, DUK_COMPILE_SHEBANG + DUK_COMPILE_FUNCTION + DUK_COMPILE_STRICT, buffer, bytesRead);
  free(buffer);
  if (message != NULL) free(message);
  return ret;
}

int load_main_script (duk_context *ctx, const char *filename) {
  char *message = NULL;
  char *buffer = NULL;
  size_t bytesRead = 0;
  duk_int_t ret = c_read_script_file(filename, &message, &buffer, &bytesRead);
  if (ret) {
    if (message != NULL) fprintf(stderr, "%s\n", message);
    if (message != NULL) free(message);
    if (buffer != NULL) free(buffer);
    exit(ret);
  }
  // Create a function from the file buffer. Leaves it on the stack.
  duk_push_string(ctx, filename);
  ret = duk_pcompile_lstring_filename(ctx, DUK_COMPILE_SHEBANG + DUK_COMPILE_FUNCTION + DUK_COMPILE_STRICT, buffer, bytesRead);
  free(buffer);
  if (message != NULL) free(message);
  return ret;
}

/**
* Footnotes
*
* [1] These hacks are needed because Windows mutates files opened in text mode
*     (which stdin and stdout are) by replacing all \n with \r\n. I want
*     binary identical output regardless of the host environment dammit.
*/
