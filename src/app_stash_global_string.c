#include "duktape/duktape.h"
#include "app_stash_global_string.h"
void app_stash_global_string(duk_context *ctx, char *name) {
  duk_push_global_stash(ctx); // summon the magic stash object
  duk_get_global_string(ctx, name);
  duk_put_prop_string(ctx, -2, name); // save as property on magic stash object
  duk_pop(ctx); // Disappear the stash
}
