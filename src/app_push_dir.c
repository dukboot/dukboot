#include "duktape/duktape.h"
#include <dirent.h>
#include "app_push_dir.h"
int app_push_dir (duk_context *ctx) {
  // Safety check filename argument
  app_assert_safe_path(ctx);
  const char *dirname = duk_get_string(ctx, -1);
  // Construct array
  duk_idx_t dir_idx = duk_push_array(ctx);
  // Shove filenames into array
  struct dirent *dir;
  int i = 0;
  int ii = 0;
  DIR *d = opendir(dirname);
  if (d) {
    while (i < 100) {
      dir = readdir(d);
      if (dir == NULL) break;
      if (strcmp(dir->d_name, ".")  != 0 &&
          strcmp(dir->d_name, "..") != 0) {
        duk_push_string(ctx, dir->d_name);
        duk_put_prop_index(ctx, dir_idx, ii);
        ii++;
      }
      i++;
    }
    closedir(d);
  }
  return 1;
}
