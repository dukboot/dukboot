#include "duktape/duktape.h"
#include "netLib/netSocket.h"

#include "bindings_netLib.h"
const int port = 8192;
const int buffsize = 2000;

void push_bindings_netLib(duk_context *ctx) {
  duk_push_c_function(ctx, wrap_netInit, 0);
  duk_put_prop_string(ctx, -2, "netInit");
  duk_push_c_function(ctx, wrap_netStop, 0);
  duk_put_prop_string(ctx, -2, "netStop");
  duk_push_c_function(ctx, wrap_netInitSocket, 0);
  duk_put_prop_string(ctx, -2, "netInitSocket");
  duk_push_c_function(ctx, wrap_netConnect, 3);
  duk_put_prop_string(ctx, -2, "netConnect");
  duk_push_c_function(ctx, wrap_netDisconnect, 1);
  duk_put_prop_string(ctx, -2, "netDisconnect");
  duk_push_c_function(ctx, wrap_netListen, 2);
  duk_put_prop_string(ctx, -2, "netListen");
  duk_push_c_function(ctx, wrap_netAccept, 2);
  duk_put_prop_string(ctx, -2, "netAccept");
  duk_push_c_function(ctx, wrap_netIsDataPending, 1);
  duk_put_prop_string(ctx, -2, "netIsDataPending");
  duk_push_c_function(ctx, wrap_netSend, 2);
  duk_put_prop_string(ctx, -2, "netSend");
  duk_push_c_function(ctx, wrap_netRecv, 2);
  duk_put_prop_string(ctx, -2, "netRecv");
  duk_push_c_function(ctx, wrap_netSocketStruct, 1);
  duk_put_prop_string(ctx, -2, "netSocketStruct");
}

duk_ret_t wrap_netSocketStruct(duk_context *ctx) {
  netSocket *socket = duk_get_pointer(ctx, 0);
  duk_push_object(ctx);
  duk_push_int(ctx, socket->number);       // the socket descriptor from the socket() call
  duk_put_prop_string(ctx, -2, "number");
  duk_push_boolean(ctx, socket->inuse);    // 1 for active, 0 for not active
  duk_put_prop_string(ctx, -2, "inuse");
  duk_push_int(ctx, socket->retval);       // all functions will update this variable with the function return value
  duk_put_prop_string(ctx, -2, "retval");
  // TODO: push struct timeval timeout; // timeout value
  // TODO: push struct sockaddr_in addr; // connector's address information
  // struct sockaddr_in {
  //     sa_family_t    sin_family; /* address family: AF_INET */
  //     in_port_t      sin_port;   /* port in network byte order */
  //     struct in_addr sin_addr;   /* internet address */
  // };
  //
  // /* Internet address. */
  // struct in_addr {
  //     uint32_t       s_addr;     /* address in network byte order */
  // };

  return 1;
}

duk_ret_t wrap_netInit(duk_context *ctx) {
  // Start the netLib library
  if (netInit() == -1) {
    return duk_generic_error(ctx, "netInit error");
  }
  return 0;
}

duk_ret_t wrap_netStop(duk_context *ctx) {
  // Start the netLib library
  if (netStop() == -1) {
    return duk_generic_error(ctx, "netStop error");
  }
  return 0;
}

duk_ret_t wrap_netInitSocket(duk_context *ctx) {
  netSocket *socket;
	// Initiate the sockets
  socket = netInitSocket();
  duk_push_pointer(ctx, (void *) socket);
  // wrap_netSocketStruct(ctx, socket);
  return 1;
}

duk_ret_t wrap_netConnect(duk_context *ctx) {
  netSocket *netSock = duk_get_pointer(ctx, 0);
  const char *address = duk_get_string(ctx, 1);
  unsigned short port = duk_get_int(ctx, 2);
  if (netConnect(netSock, address, port) == -1) {
    return duk_generic_error(ctx, "netConnect error");
  }
  return 0;
}

duk_ret_t wrap_netDisconnect(duk_context *ctx) {
  netSocket *netSock = duk_get_pointer(ctx, 0);
  if (netDisconnect(netSock) == -1) {
    return duk_generic_error(ctx, "netDisconnect error");
  }
  return 0;
}

duk_ret_t wrap_netListen(duk_context *ctx) {
  netSocket *listensocket = duk_get_pointer(ctx, -2);
  unsigned short port = duk_get_int(ctx, -1);
	if (netListen(listensocket, port) == -1) {
    return duk_generic_error(ctx, "netListen error");
  }
  return 0;
}

duk_ret_t wrap_netAccept(duk_context *ctx) {
  netSocket *listenSock = duk_get_pointer(ctx, -2);
  netSocket *netSock = duk_get_pointer(ctx, -1);
	int ret = netAccept(listenSock, netSock);
	if (ret == -1) {
    return duk_generic_error(ctx, "netAccept error");
  }
  duk_push_boolean(ctx, ret);
  return 1;
}

duk_ret_t wrap_netIsDataPending(duk_context *ctx) {
  netSocket *netSock = duk_get_pointer(ctx, -1);
	int ret = netIsDataPending(netSock);
  duk_pop(ctx);
  duk_push_boolean(ctx, ret);
  return 1;
}

duk_ret_t wrap_netSend(duk_context *ctx) {
  netSocket *netSock = duk_get_pointer(ctx, 0);
  duk_size_t bytes = 0;
  const char *data = duk_get_buffer_data(ctx, 1, &bytes);
	int ret = netSend(netSock, data, (int) bytes);
  if (ret == -1) {
    return duk_generic_error(ctx, "netSend error");
  }
  duk_pop_2(ctx);
  duk_push_int(ctx, ret);
  return 1;
}

duk_ret_t wrap_netRecv(duk_context *ctx) {
  netSocket *netSock = duk_get_pointer(ctx, 0);
  duk_size_t bytes = 0;
  char *data = duk_get_buffer_data(ctx, 1, &bytes);
	int ret = netRecv(netSock, data, (int) bytes);
  if (ret == -1) {
    return duk_generic_error(ctx, "netRecv error");
  }
  duk_pop_2(ctx);
  duk_push_int(ctx, ret);
  return 1;
}

// TODO: netSetTimeout
// TODO: netSetOption
//
