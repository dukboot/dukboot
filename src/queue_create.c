#include "duktape/duktape.h"
#include "queue_create.h"
int queue_create(duk_context *ctx, char *name) {
  // Create a top-level Array on the heap stash with the given name
  duk_push_heap_stash(ctx);
  duk_push_array(ctx);
  duk_put_prop_string(ctx, -2, name);
  duk_pop(ctx);
  return 0;
}
