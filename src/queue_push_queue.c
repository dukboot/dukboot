#include "duktape/duktape.h"
#include "queue_push_queue.h"
int queue_push_queue(duk_context *ctx, char *name) {
  duk_push_heap_stash(ctx);
  duk_get_prop_string(ctx, -1, name);
  duk_remove(ctx, -2);
  return 0;
}
