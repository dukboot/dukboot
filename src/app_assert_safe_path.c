#include "duktape/duktape.h"
#include "app_assert_safe_path.h"
duk_int_t app_assert_safe_path (duk_context *ctx) {
  // Safety check filename argument
  duk_require_string(ctx, -1);
  const char *filename = duk_get_string(ctx, -1);
  if (filename[0] == '\0') {
    return duk_error(ctx, DUK_ERR_ERROR, "Path cannot be empty.");
  } else if (filename[0] == '/') {
    return duk_error(ctx, DUK_ERR_ERROR, "Path should not start with '/' to reference the root directory: %s", filename);
  } else if (strcmp(filename, "..") == 0) {
    return duk_error(ctx, DUK_ERR_ERROR, "Path cannot be the parent directory '..'");
  } else if (strstr(filename, "../") != NULL) {
    return duk_error(ctx, DUK_ERR_ERROR, "Path should not contain '..' to reference parent directories: %s", filename);
  } else if (strncmp(filename, "./", 2) == 0) {
    // (this restriction isn't about safety, just consistancy and KISS)
    return duk_error(ctx, DUK_ERR_ERROR, "Path should not start with extraneous './' reference the current directory: %s", filename);
  } else if (strstr(filename, "\\") != NULL) {
    // No paths with non-standard file separators.
    return duk_error(ctx, DUK_ERR_ERROR, "Path should not contain '\\' character, only use '/' to separate paths: %s", filename);
  } else {
    // Whew! By this point, I think we're only left with canonical representations.
    return 0; // 0 results on stack
  }
}
