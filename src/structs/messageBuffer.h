typedef struct messageBuffer {
  int owner;
  size_t last;
  size_t size;
  char* buffer;
} messageBuffer;

typedef struct messageBufferA {
  size_t last;
  size_t size;
  messageBuffer* items;
} messageBufferA;

#define messageBufferFromRaw(ptr) ((messageBuffer*) ((int)ptr - offsetof(messageBuffer, buffer)))
