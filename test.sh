#!/bin/sh
DUKBOOT="${1:-./dukboot}"
NUM_CORES="$(grep -c ^processor /proc/cpuinfo)"
echo "$NUM_CORES cores"
EXPECT_ONE='[12,"index.html"]'
EXPECT="$EXPECT_ONE"
for n in $(seq 2 $((NUM_CORES * 1))); do
  EXPECT="$EXPECT
$EXPECT_ONE"
done
RESULT="$($DUKBOOT examples/tests/parent.js)"
echo "$EXPECT"
echo "---"
echo "$RESULT"
test "$RESULT" = "$EXPECT"
