# dukboot

An experimental alternative to Node.js. `"duk"` because it uses the [Duktape](http://duktape.org/) ECMAScript engine, and `"boot"` because I want to be able to [boot it](https://github.com/wmhilton/boots) directly on bare metal.

## See also these tools

- [dukroll](https://gitlab.com/dukboot/dukroll) A module bundler for `dukboot`.

## Existing projects that are way farther along than this one

- [seaduk](https://github.com/creationix/seaduk)
- [nojs](https://github.com/chrisdickinson/nojs)
- [NodeOS](https://github.com/NodeOS/NodeOS)
- [runtime.js](http://runtimejs.org/)

### Then why does this exist...?

This way I can try really crazy stuff in a zero pressure environment.
Once I've made enough mistakes to scrap it and do a full rewrite, maybe
I'll team up with some peeps.

Also, I don't want to tell some really really smart people that they are
doing it all wrong, that they're simply repeating the mistakes of Node, that
the majority of software written today is completely, utterly insecure, and
the world is doomed unless we change our programming model completely. Bold
claims require bold whatever whatever and I need a space to fully work through
my ideas on how to fix the world, see what the impact is, and why they won't work
so we're all still doomed.

### How?

Pragmatism. Sure, I'd love a JavaScript OS that runs directly on bare metal
or a hypervisor, but that's simply ONE idea. To start, I'm doing the simpler
object of targeting the Alpine Linux Docker image. That has most of the
size advantage of a unikernel anyway. And yes, that totally means I'm cheating
and not using [boots](https://github.com/wmhilton/boots) at all. For now.

### What's the philosophy behind it?

Wait... do I need a manifesto or something? Probably, eventually. For now, I'll
just leave some quotes from this wise collector of quotes: http://quotes.cat-v.org/programming/

> The cheapest, fastest, and most reliable components are those that aren’t there.
> — Gordon Bell

> Simplicity is prerequisite for reliability.
> — Edsger W. Dijkstra

> If you’re willing to restrict the flexibility of your approach, you can almost always do something better.
> — John Carmack

> Correctness is clearly the prime quality. If a system does not do what it is supposed to do, then everything else about it matters little.
> — Bertrand Meyer

> You can’t trust code that you did not totally create yourself.
> — Ken Thompson

> Compatibility means deliberately repeating other people’s mistakes.
> — David Wheeler

> Program testing can be a very effective way to show the presence of bugs, but is hopelessly inadequate for showing their absence.
> — Edsger W. Dijkstra

> The competent programmer is fully aware of the limited size of his own skull. He therefore approaches his task with full humility, and avoids clever tricks like the plague.
> — Edsger W. Dijkstra

## So what IS this?

A C program. I eventually caved and wrote a Makefile for it, so now it has a dependency on `make`.
But it's a really simple Makefile so hopefully I can replace it with small build script
in the future. I'm using [makeheaders](http://www.hwaci.com/sw/mkhdr/) to avoid having
to write any header files. (It's a truly wonderful gem of a program and I think
more people should use it!) So mainly the Makefile is just used to select which
libraries get linked depending on the target platform.

It uses a couple parts of the C standard library, so for now you have to build it against something.
On Alpine Linux, that is `musl` and on Ubuntu Linux that's `glibc` I think.

Both Ubuntu and Alpine Linux are supported.
I've got a fancy Gitlab Pipeline that compiles and tests the program on both
Ubuntu and Alpine Linux. Mostly because it would slow things down if I had to
always compile against Alpine Linux when my dev environment is Bash for Windows.

I also have compiled binaries for Windows now thanks to this [incredible Docker image](https://hub.docker.com/r/thewtex/cross-compiler-windows-x64)!

## Downloads

Here's some precompiled binaries courtesy of Gitlab CI:

- Download the [dukboot binary for Ubuntu](https://gitlab.com/dukboot/dukboot/builds/artifacts/master/download?job=compile-for-ubuntu)
- Download the [dukboot binary for Alpine Linux](https://gitlab.com/dukboot/dukboot/builds/artifacts/master/download?job=compile-for-alpine)
- Download the [dukboot binary for Windows](https://gitlab.com/dukboot/dukboot/builds/artifacts/master/download?job=compile-for-windows)

---

## Agenda

- [ ] An event loop. I *think* this portion can be written in JavaScript, with just
a bit of C assist.

Like `libuv` but... not `libuv`. Something small and manageable.

I was very tempted to use `nanomsg` to create a queue (still might if I can ever get it
to compile). But I am currently thinking [OpenMP](https://en.wikipedia.org/wiki/OpenMP#Thread_creation) is the dead simplest
way to go. Combine that with `duk_suspend` and `duk_resume` for C code and there
you have it. Then we just need to write an ethernet driver in assembly. ;)

- [ ] Pure `musl`? Or whatever? We need to eventually isolate all OS kernel calls since we won't need no stinkin' operating system.

- [x] Networking! I've got a very basic networking library added that appears to work so far.


# API

This section has gotten too big, so I moved it to the [wiki](https://gitlab.com/dukboot/dukboot/wikis/API/API)

## ~~Module System~~ Sandbox

A wise man once said,

  <blockquote class="twitter-tweet" data-conversation="none" data-lang="en"><p lang="en" dir="ltr">looks interesting!<br>to be honest, skip the module system. just run a single file and I&#39;ll use browserify.<br>but process/actor model: BIG YES</p>&mdash; Dominic Tarr (@dominictarr) <a href="https://twitter.com/dominictarr/status/868585203286949888">May 27, 2017</a></blockquote>
  <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

So I've decided to leave modules as a "trivial exercise for the user". Just kidding!
There are two excellent ways to approach modules:

The first is to use a module bundler. Want to write ES6 modules? Look no further than [dukroll](https://gitlab.com/dukboot/dukroll).

However, bundling everything into one file though loses us some opportunity to sandbox
third party code. And what about dynamic module loading? We'll need that if
we ever want to do hot module reloading. This leads us to the second way, which
needs to enable us to load & compile code, without running `eval`, and delay executing it / execute it in a
controlled environment. I haven't nailed down the details yet, but that is what this
section is about.

### Function Files
In dukboot, the base unit of code is not modules, but what I call "function files".
A function file is very simple: it's a file that contains exactly one single JavaScript function.

### sandbox.compileTrustedFunction(String filepath) => Function
The argument to `compile` must be a path to a "function file" as defined above.
The path is always relative to the filename of the calling function. It expects
that file to contain a single JavaScript function, and it will be treated as a
"strict mode" function because that's the only kind anyone should be writing
these days. It is a "trusted" function because it is compiled in the same global scope & heap as the calling function.
I'm planning on adding ways to instantiate functions in a separate scope and heap.

To keep you from writing spaghetti code, function files can
only call compile on other function files in the same directory or a subdirectory.
In Node, it is common to see users attept to create deep folder hierarchies and
then break out of them with `require('./../../../foo/bar/baz')`. This is an
extremely poor practice and makes your code fragile and hard to refactor. If
you find yourself wanting to do this, dukboot won't let you. Instead, use a sane
method like dependency injection. That is to say, require 'baz' somewhere
higher up and pass it as an argument to functions that need it. In fact, the
entire core module system works this way: your top-level function receives
an `imports` object as it initial argument, which looks something like this:

```
{
  "main": [object Object],
  "stdio": [object Object],
  "thread": [object Object],
  "netLib": [object Object],
  ...
}
```

### supersocket - TBD

I would like to have some kind of suped up socket like zeromq or nanomsg built into dukboot, so that you can start building advanced network topologies right away without worrying about protocols. However, I can't get either of them to compile yet.

## Event Loop

Everyone else is using `libuv`. But libuv actually does *more* than I think
is actually necessary. And using it would means that `dukboot` would have exactly the same
bugs / vulnerabilities as all the other stuff out there. So why not be crazy
and try something different?

A lot of JS programmers are mystified by the Event Loop. This is largely because
its existance is *implicit*, and you can go for years writing JavaScript and
never even be aware of it. It is used via functions like `setImmediate()` and
`process.nextTick()` and `requestAnimationFrame()` and `requestIdleCallback()`
which are all poorly named\* and obscure what's really going on. Once you understand
it, it's extremely simple! It's a cooperative scheduler. Callbacks are put in a queue and
get executed in order for the most part. Some things, like animation frames in
the browser or I/O in node, get executed out-of-order to satisfy constraints:
animationFrame callbacks need to happen between window paint events for instance
and I/O callbacks cannot be called until the data they are asking for arrives.
But at its heart, it's just a handful of queues that the scheduler picks from.
So I figure... why not make that explicit? Why not literally represent the
queues as a JS arrays, so you can write your own scheduling logic? You can see
my initial fooling around with this idea in the `examples/time/` directory.
Conceptually, you would have one Duktape.Thread be the "scheduler" and have a
while loop that pops callbacks from the queue(s) and run them until the queue is
empty. The callbacks themselves, other Duktape Threads, or the C code will insert
new callbacks into the queues so the scheduler thread has stuff to execute.

\* except requestAnimationFrame, which actually does what the name suggests

### Queues.timers (name subject to change)
Right now this is the only queue. I'm storing it in the heap-stash. The code in
`examples/time/` shows you can actually pull off the whole event loop in JS without
using any C code. This may prove a totally awesome idea or a completely
terrible one; I don't know yet.

## Disk I/O

I'll provide access to files, but it will be with an interface more like `abstract-blob-store` than `fs`. Will also provide something like `abstract-chunk-store` for direct block level access.

## Device Drivers

### mmap - map a JS TypedArrayBuffer to e.g. VGA display memory
