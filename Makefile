CC     ?= gcc
CFLAGS = -fopenmp -Ideps -Os -pedantic -std=c99 -Wall -fstrict-aliasing -fomit-frame-pointer -D_GNU_SOURCE
LDFLAGS = -static -static-libgcc -lm

HOST_OS ?= $(OS)
TARGET_OS ?= $(OS)

ifeq ($(HOST_OS),Windows_NT)
MAKEHEADERS = makeheaders.exe
else
MAKEHEADERS = makeheaders
endif

ifeq ($(TARGET_OS),Windows_NT)
BIN    = dukboot.exe
CFLAGS += -DMS_WINDOWS
LDFLAGS += -lwsock32
else
BIN    = dukboot
CFLAGS += -DUNIX
endif

SRC     = $(wildcard src/*.c)
HEADERS = $(SRC:.c=.h)
OBJS    = $(SRC:.c=.o)
DEPS    = $(wildcard deps/duktape/*.c) deps/netLib/netSocket.c
DEPOBJS = $(DEPS:.c=.o)

main: $(MAKEHEADERS) $(HEADERS) $(DEPOBJS) $(OBJS)
	$(CC) $(CFLAGS) -o $(BIN) $(OBJS) $(DEPOBJS) $(LDFLAGS)

$(MAKEHEADERS): makeheaders.c
	gcc makeheaders.c -o makeheaders

%.h: %.c
	./makeheaders src/*.c src/structs/*.h

%.o: %.c %.h
	$(CC) $< -c -o $@ $(CFLAGS)

clean:
	$(foreach c, $(BINS), $(RM) $(c);)
	$(RM) $(OBJS) $(HEADERS)

cleaner: clean
	$(RM) $(wildcard deps/*/*.o)
	$(RM) dukboot dukboot.exe makeheaders makeheaders.exe

test:
	@./test.sh

.PHONY: test all clean install uninstall cleaner
