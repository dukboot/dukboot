function (imports) {
  var env = imports.environ;
  var argv = imports.argv;
  var netLib = imports.netLib
  var Queues = imports.Queues;
  var timers = Queues.timers;
  var compile = imports.sandbox.compileTrustedFunction;
  var print = imports.log.log

  if (imports.thread.id() !== 0) return

  var queue = []

  function schedule (task) {
    queue.push(task)
  }

  function tick () {
    if (queue.length === 0) return false;
    // Dequeue
    var task = queue.shift();
    // Run it
    task[0].apply(null, task.slice(1))
    return true
  }

  var port = 8080;
  var buffsize = 2000;

  var buffer = Uint8Array.allocPlain(1000);
  var tint = 0;

  print("\nListening on port " + port);

  // Start the netLib library
  netLib.netInit();

  // Initiate the sockets
  function duplexStream (clientsocket) {
    var source = function read (end, cb) {
      if (end) {
        // Disconnect the client
        print("-- Source received end message.");
        netLib.netDisconnect(clientsocket);
        return
      }
      var buffer = Uint8Array.allocPlain(1000);
      // Check for data to receive and print if there was something
      try {
        var bytesRead = netLib.netRecv(clientsocket, buffer);
        if (bytesRead > 0) {
          var croppedBuffer = buffer.subarray(0, bytesRead);
          print("-- Read " + bytesRead + " bytes.");
          return schedule([cb, null, croppedBuffer])
        } else {
          return schedule([read, end, cb])
        }
      } catch (err) {
        // an error means the other end hung up
        print("-- Source hung up!");
        netLib.netDisconnect(clientsocket);
        return schedule([cb, true])
      }
    }
    var sink = function reader (read) {
      read(null, function next (end, data) {
        if (end) {
          // Disconnect the client
          print("-- Sink received end message.");
          netLib.netDisconnect(clientsocket);
          return
        }
        // Send data to the client
        print(data.length)
        try {
          var bytesWritten = netLib.netSend(clientsocket, data);
          print("-- Sent " + bytesWritten + " bytes.");
          return schedule([read, null, next])
        } catch (err) {
          // Socket no longer there (client disconnected from us)
          print("-- Sink hung up!");
          return schedule([read, true])
        }
      })
    }
    return {source: source, sink: sink}
  }

  var clientsocket = netLib.netInitSocket();
  var listensocket = netLib.netInitSocket();

  // Set the server to listen
  netLib.netListen(listensocket, port);

  print("\n");

  var connections = 0
  var reads = 0

  // Main program loop
  while (true) {
    // Check for an incoming connection
    if (netLib.netAccept(listensocket, clientsocket)) {
      // A client has connected!
      print("Hello new client!");

      var stream = duplexStream(clientsocket);

      // basic echo server
      // stream.source(null, function yayIncoming (end, data) {
      //   print(new TextDecoder().decode(data));
      //   reads++;
      //   stream.sink(reads > 4, data)
      //   schedule([stream.source, null, yayIncoming])
      // })
      stream.sink(function (abort, cb) {
        stream.source(abort, function (end, data) {
          if (end) return cb(end, data);
          var text = new TextDecoder().decode(data);
          print(text);
          // Upon receiving "fin" close the connection
          if (text === 'fin') {
            stream.source(true)
            return cb(true)
          }
          return cb(end, data);
        })
      })

      while (tick());

      // Each thread serves 2 requests before getting tired.
      if (++connections == 2) break;
    }
  }

  print("That's enough for now! Bye!");
  netLib.netDisconnect(listensocket);

  // Stop the netLib library
  netLib.netStop();

  return
}
