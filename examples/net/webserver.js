function (imports) {
  var env = imports.environ;
  var argv = imports.argv;
  var Queues = imports.Queues;
  var timers = Queues.timers;
  var compile = imports.sandbox.compileTrustedFunction;
  var print = imports.log.log
  var netLib = imports.netLib

  var port = 8080;
  var buffsize = 2000;

  /* Default HTTP page with HTTP headers */
  var webpage =
  "HTTP/1.0 200 OK\n" +
  "Server: BareMetal (http://www.returninfinity.com)\n" +
  "Content-type: text/html\n" +
  "\n" +
  "<!DOCTYPE html>\n" +
  "<html>\n" +
  "<head>\n" +
  "<title>Hello world</title>\n" +
  "</head>\n" +
  "<body>\n" +
  "Hello World!\n" +
  "</body>\n" +
  "</html>\n";

  var webpageBuffer = Uint8Array.allocPlain(webpage);
  print(webpageBuffer.length)
  var buffer = Uint8Array.allocPlain(1000);
  var tint = 0;

	print("Web Server demo for netLib v0.4\nListening on port " + port + "\n");

	// Start the netLib library
  netLib.netInit();

  // Initiate the sockets
  var clientsocket = netLib.netInitSocket();
  var listensocket = netLib.netInitSocket();

  // Set the server to listen
  netLib.netListen(listensocket, port);

  print("\n\n");

  var connections = 0

  // Main program loop
  while (true) {
    // Check for an incoming connection
    if (netLib.netAccept(listensocket, clientsocket)) {
      // A client has connected!
      print("\nHello.");

      // Check for data to receive and print if there was something
      tint = netLib.netRecv(clientsocket, buffer);
      if (tint > 0) {
        print("\nReceived " + tint + " bytes.");
      }
      print(new TextDecoder().decode(buffer));

			// Send data to the client
			tint = netLib.netSend(clientsocket, webpageBuffer);
			print("\nSent " + tint + " bytes.");

			// Disconnect the client
      netLib.netDisconnect(clientsocket);
      print("\nGoodbye.\n\n");

      // Each thread serves 2 requests before getting tired.
      if (++connections == 2) break;
    }
  }

  print("\nThat's enough for now! Bye!\n\n");
  netLib.netDisconnect(listensocket);

  // Stop the netLib library
  netLib.netStop();

  print("\n");
  return
}
