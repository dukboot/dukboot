function (imports) {
  if (typeof global === 'undefined') {
    (function () {
      var global = new Function('return this;')();
      Object.defineProperty(global, 'global', {
        value: global,
        writable: true,
        enumerable: false,
        configurable: true
      });
    })();
  }
  // A stupid way to make dependencies available, that will
  // probably necessary to be compatible with ye olde node.
  global.dir = imports.modules.dir
  var testme = imports.sandbox.compileTrustedFunction('subexample/grandchild.js');
  imports.stdio.print(JSON.stringify(testme(3, 4)) + '\n');
  return
}
