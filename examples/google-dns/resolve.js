function (imports) {
  if (imports.thread.id() > 0) return;
  var netLib = imports.netLib;
  var schedule = imports.scheduler.append;
  var log = imports.log.log;
  var print = imports.stdio.print;

  // Start the netLib library
  netLib.netInit();

  var clientsocket = netLib.netInitSocket();

  netLib.netConnect(clientsocket, 'dns.google.com', 80);

  var stream = imports.streams.TCPDuplexStream(clientsocket);
  var UTF8decoder = new TextDecoder('utf-8')
  var UTF8encoder = new TextEncoder('utf-8')

  // Setup echo server
  stream.sink(function (end, cb) {
    var request = 'GET /resolve?name=dns.google.com&type=A HTTP/1.1' + '\r\n' +
                  'Host: dns.google.com' + '\r\n' +
                  'Accept: application/json' + '\r\n\r\n'
    var bytes = UTF8encoder.encode(request);
    cb(null, bytes)
  })
  stream.source(null, function (err, buffer) {
    var text = UTF8decoder.decode(new Uint8Array(buffer));
    // TODO: use fancier HTTP parser?
    // For now: sloppy HTTP body parser!
    try {
      text = text.slice(text.indexOf('{'), text.lastIndexOf('}')+1)
      log(text);
      var json = JSON.parse(text);
      log(json);
      print(json.Answer[0].data + '\n');
    } catch (err) {
      log('Could not parse JSON response')
    }
    // Stop the netLib library
    netLib.netStop();
  })
}
