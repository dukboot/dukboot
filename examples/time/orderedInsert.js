function () {
  return function (compareFunction, arr, element) {
    // Lol. Efficiency could be improved, but this is most obviously correct.
    arr.push(element);
    arr.sort(compareFunction);
  }
}
