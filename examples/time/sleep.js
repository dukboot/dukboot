function (imports) {
  var env = imports.main.environ();
  var argv = imports.main.argv();
  var thread = imports.thread;
  var Queues = imports.Queues;
  var timers = Queues.timers;
  var compile = imports.sandbox.compileTrustedFunction;
  var printTime = compile('printTime.js')(imports.log.log);
  var orderedInsert = compile('orderedInsert.js')();
  var byMstimestampAscending = function (a, b) {
    return a.t - b.t;
  }
  var scheduleCallback = function (mstimestamp, callback) {
    orderedInsert(byMstimestampAscending, timers, {t: mstimestamp, cb: callback});
  }
  printTime();
  var now = Math.trunc(Date.now() / 1000) * 1000;
  // Queue some events
  scheduleCallback(now + 4000, printTime);
  scheduleCallback(now + 2000, printTime);
  scheduleCallback(now + 8000, printTime);
  // It is a **very** basic event loop.
  while (1) {
    if (timers.length === 0) break;
    // Dequeue
    var event = timers.shift();
    // Schedule next event (TODO: allow/handle waking up prematurely via an interupt)
    thread.sleep(event.t - Date.now());
    // Run synchronously (for now)
    event.cb();
  }
  return
}
