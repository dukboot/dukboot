function (o) {
  if (o.thread.id() === 0) {
    // This runs in about 550ms on my machine.
    // Interestingly, I get the same performance buffering "manually" like this,
    // as I do by leaving stdout buffering enabled in the C layer and running
    // the `stdout-unbuffered` demo. Using both C buffering and manual buffering
    // doesn't provide any noticeable speedup either.
    //
    // So why add the complexity of buffering at the C layer if buffering in
    // the JS layer is equally performant? For now then I'm disabling buffering
    // in the C layer. Moving output buffering into the JS layer allows you the
    // developer more flexibility and power to tweak the heck out of it for performance.
    // And more importantly, your output is actually output when you put it out!
    // None of this "Oh, well if you were serious, you need to add process.stdout.flush()
    // after your console.log" which causes beginners so much pain.
    //
    // Buffering is an optimization. Do it when writing becomes a bottleneck, not
    // by default on fricking everything.
    var c;
    var chars = []
    for (var i = 0x20; i < 0x7E; i++) {
      for (var n = 0; n < 10000; n++) {
        chars.push(i)
      }
      o.stdio.putbuffer(new Uint8Array(chars))
      chars = []
    }
  }
}
