function unicodeStream (read) {
  var UTF8decoder = new TextDecoder('utf-8')
  function howManyBytes (firstByte) {
    if (firstByte > 254) throw Error("Number too large to be a byte: " + firstByte)
    if (firstByte >> 7 === 0) return 1
    if (firstByte >> 5 === 0b110) return 2
    if (firstByte >> 4 === 0b1110) return 3
    if (firstByte >> 3 === 0b11110) return 4
  }
  return function unicodeThrough (end, cb) {
    if (end) return cb(end)
    var length = null;
    var chars = [];
    read(null, function next(end, byte) {
      if (end) return cb(end)
      length = length || howManyBytes(byte)
      chars.push(byte)
      var char = UTF8decoder.decode(new Uint8Array(chars))
      if (chars.length === length) return cb(null, char)
      read(null, next)
    })
  }
}
