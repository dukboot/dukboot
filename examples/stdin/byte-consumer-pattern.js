function (o) {
  // For fun, let every thread race to read from stdin. Quite amusing.
  // Results will (probably) be printed out of order.
  var thread = o.thread.id()
  var c
  while (true) {
    c = o.stdio.getUint8()
    if (c instanceof Error) break
    o.stdio.print(thread + ': ' + c)
  }
}
