function (imports) {
  var queue = []

  function schedule (task) {
    queue.push(task)
  }

  function read (end, cb) {
    if (end) return cb(end)
    var c = imports.stdio.getUint8()
    if (c === undefined) return schedule([read, end, cb])
    if (c instanceof Error) return cb(c)
    return schedule([cb, null, c])
  }

  function tick () {
    if (queue.length === 0) throw Error("Queue complete.");
    // Dequeue
    var task = queue.shift();
    // Run it
    task[0].apply(null, task.slice(1))
    return true
  }

  if (imports.thread.id() === 0) {
    var unicodeStream = imports.sandbox.compileTrustedFunction('unicode-stream.js')
    var readUnicode = unicodeStream(read)
    var c
    var chars = []
    readUnicode(null, function next(end, char) {
      if (end) throw end
      imports.log.log(char)
      schedule([readUnicode, null, next])
      // readUnicode(null, next)
    })
    var i = 0;
    try {
      while (true) {
        imports.log.log("Tick count: " + i)
        tick()
        i++
      }
    } catch (e) {
      imports.log.log(e)
    }
  }
}
