function (o) {
  if (o.thread.id() === 0) {
    // On my system, this takes 18.0 seconds to run!
    // This is because by default we are not buffering output.
    // See `stdout-buffered` example next.
    var c;
    for (var i = 0x20; i < 0x7E; i++) {
      for (var n = 0; n < 10000; n++) {
        o.stdio.putUint8(i)
      }
    }
  }
}
