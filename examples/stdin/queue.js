function (o) {
  var queue = []

  function schedule (task) {
    queue.push(task)
  }

  function read (end, cb) {
    if (end) return cb(end)
    var c = o.stdio.getUint8()
    if (c === undefined) return schedule([read, end, cb])
    if (c instanceof Error) return cb(c)
    return schedule([cb, null, c])
  }

  function tick () {
    if (queue.length === 0) throw Error("Queue complete.");
    // Dequeue
    var task = queue.shift();
    // Run it
    task[0].apply(null, task.slice(1))
    return true
  }

  if (o.thread.id() === 0) {
    var c
    var chars = []
    read(null, function next(end, data) {
      if (end == true) return
      if (end) throw end
      o.stdio.print(String.fromCharCode(data))
      // schedule([read, null, next])
      read(null, next)
    })
    var i = 0;
    try {
      while (true) {
        o.stdio.print("Tick count: " + i)
        tick()
        i++
      }
    } catch (e) {
      o.stdio.print(e)
    }
  }
}
