function (imports) {
  // If you want to read bytes in order, I would recommend having
  // a single master thread read the bytes. You can divide the work later.
  var thread = imports.thread.id()
  if (thread === 0) {
    var c;
    while (true) {
      c = imports.stdio.getUint8()
      imports.log.log(thread + ': ' + c)
      if (c instanceof Error) break
      imports.thread.sleep(100)
    }
  }
}
