function (imports) {
  // If you want to read bytes in order, I would recommend having
  // a single master thread read the bytes. You can divide the work later.
  var thread = imports.thread.id()
  if (thread === 0) {
    var UTF8decoder = new TextDecoder('utf-8')
    var c
    var buffer = new Uint8Array(8);
    while (true) {
      c = imports.stdio.getbuffer(buffer)
      if (c instanceof Error) break
      if (c === undefined) imports.thread.sleep(100)
      if (c > 0) {
        imports.log.log(thread + ': read ' + c + ' bytes')
        imports.log.log(UTF8decoder.decode(buffer.subarray(0, c)))
      }
    }
  }
}
