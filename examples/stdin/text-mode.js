function (o) {
  // If you want to read bytes in order, I would recommend having
  // a single master thread read the bytes. You can divide the work later.
  if (o.thread.id() === 0) {
    var c;
    var chars = [];
    while (true) {
      c = o.stdio.getUint8()
      if (c !== undefined) chars.push(c)
      if (c instanceof Error) break
    }
    o.stdio.print(new TextDecoder().decode(new Uint8Array(chars)))
  }
}
