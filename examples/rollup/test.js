// To run this example, you'll need to use
// dukroll (https://gitlab.com/dukboot/dukroll)
import pull from 'pull-stream'
import dukboot, { thread, stdio } from 'dukboot'

if (thread.id() === 0) {
  stdio.print(dukboot.main.argv()) // show off 'default' exports
  var c = pull.count()
  c(null, stdio.print)
  c(null, stdio.print)
  c(null, stdio.print)
  c(null, stdio.print)
}
