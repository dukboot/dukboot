function app (imports) {
  if (imports.thread.id() === 0) {
    imports.streams.ByteDuplexStream.sink(imports.streams.ByteDuplexStream.source)
  }
}
