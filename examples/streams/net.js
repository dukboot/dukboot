function app (imports) {
  if (imports.thread.id() === 0) {
    var server = imports.streams.TCPDuplexSourceStream({port: 8080, host: 'localhost'});
    imports.log.log('test 1')

    var onConnection = function onConnection (err, connection) {
      imports.log.log('test 2')
      // Setup echo server
      connection.sink(connection.source)
      imports.scheduler.append([server, null, onConnection])
    }
    server(null, onConnection);
  }
}
