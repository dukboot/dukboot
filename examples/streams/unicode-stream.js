function app (imports) {
  if (imports.thread.id() === 0) {
    imports.streams.UnicodeThroughStream.encoder(imports.streams.ByteDuplexStream.sink)(
      imports.streams.UnicodeThroughStream.decoder(
        imports.streams.ByteDuplexStream.source
      )
    )
  }
}
