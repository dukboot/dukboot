function (dukboot) {
  var print = dukboot.stdio.print;
  var n = dukboot.thread.pool().length;
  var id = dukboot.thread.id();
  var buffer = dukboot.message.alloc(5);
  for (var i = 0; i < buffer.length; i++) {
    buffer[i] = id;
  }
  var s = (id + 1) % n
  print("id " + id + ": wrote " + buffer[0] + ', size=' + buffer.length);
  dukboot.message.send(s, buffer);
  if (id === 0) dukboot.message.send(s, buffer);
  buffer = undefined;
  while(buffer == undefined)
    buffer = dukboot.message.read();
  print("id " + id + ": read " + buffer[0] + ', size=' + buffer.length);
}
