function (imports) {
  if (imports.thread.id() % 2 === 0) {
    imports.thread.sleep(5000)
    imports.log.log(imports.thread.id() + ': ' + 'ha')
  } else {
    for (var i = 0; i < 5; i++) {
      imports.log.log(imports.thread.id() + ': ' + i)
      imports.thread.sleep(1000 * Math.random())
    }
  }
}
