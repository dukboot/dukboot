function (imports) {
  var netLib = imports.netLib;
  var print = imports.log.log;
  var schedule = imports.scheduler.append;

  // TCP connection stream
  return function duplexStream (clientsocket) {
    return {
      source: function read (end, cb) {
        if (end) {
          // Disconnect the client
          print("-- Source received end message.");
          netLib.netDisconnect(clientsocket);
          return
        }
        var buffer = Uint8Array.allocPlain(1000);
        // Check for data to receive and print if there was something
        try {
          var bytesRead = netLib.netRecv(clientsocket, buffer);
          if (bytesRead > 0) {
            var croppedBuffer = buffer.subarray(0, bytesRead);
            print("-- Read " + bytesRead + " bytes.");
            return schedule([cb, null, croppedBuffer])
          } else {
            return schedule([read, end, cb])
          }
        } catch (err) {
          // an error means the other end hung up
          print("-- Source hung up!");
          netLib.netDisconnect(clientsocket);
          return schedule([cb, true])
        }
      },
      sink: function reader (read) {
        read(null, function next (end, data) {
          if (end) {
            // Disconnect the client
            print("-- Sink received end message.");
            netLib.netDisconnect(clientsocket);
            return
          }
          // Send data to the client
          try {
            var bytesWritten = netLib.netSend(clientsocket, data);
            print("-- Sent " + bytesWritten + " bytes.");
            return schedule([read, null, next])
          } catch (err) {
            // Socket no longer there (client disconnected from us)
            print("-- Sink hung up!");
            return schedule([read, true])
          }
        })
      }
    }
  }
}
