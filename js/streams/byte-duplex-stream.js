function (dukboot) {
  var getUint8 = dukboot.stdio.getUint8;
  var putUint8 = dukboot.stdio.putUint8;
  var next = dukboot.scheduler.append;
  // setup
  function read (end, cb) {
    if (end) return cb(end)
    var c = getUint8()
    if (c === undefined) return next([read, end, cb])
    if (c instanceof Error) return cb(c)
    return next([cb, null, c])
  }

  function sink (read) {
    read(null, function write (err, byte) {
      if (err) return
      var c = putUint8(byte)
      if (c instanceof Error) return read(true)
      if (c === undefined) return next([read, null, write])
      return next([read, null, write])
    })
  }

  return {
    source: read,
    sink: sink
  }
}
