function (imports) {
  var netLib = imports.netLib;
  var print = imports.log.log;
  var schedule = imports.scheduler.append;
  var TCPDuplexStream = imports.streams.TCPDuplexStream;

  return function streamOfStreams (args) {
    // Start the netLib library
    netLib.netInit();

    var port = args.port;
    var host = args.host;

    var listensocket = netLib.netInitSocket();

    // Set the server to listen
    netLib.netListen(listensocket, port);

    return function read (end, cb) {
      if (end) {
        // TODO: We need a way to kill all current connections. :/
        print("That's enough for now! Bye!");
        netLib.netDisconnect(listensocket);

        // Stop the netLib library
        netLib.netStop();
        return
      }

      // Fresh new sockets for everyone! (Well, every client anyway).
      var clientsocket = netLib.netInitSocket();
      if (netLib.netAccept(listensocket, clientsocket)) {
        // A client has connected!
        print("Hello new client!");

        var stream = TCPDuplexStream(clientsocket);
        return cb(null, stream);
      } else {
        schedule([read, end, cb])
      }
    }
  }
}
