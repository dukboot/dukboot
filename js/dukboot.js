function (imports) {
  // setup
  var queue = []

  function schedule (task) {
    queue.push(task)
  }

  imports.scheduler = imports.scheduler || {}
  imports.scheduler.append = schedule

  imports.streams = {}
  imports.streams.ByteDuplexStream = imports.sandbox.compileTrustedFunction('streams/byte-duplex-stream.js')(imports)
  imports.streams.UnicodeThroughStream = imports.sandbox.compileTrustedFunction('streams/unicode-through-stream.js')(imports)
  imports.streams.TCPDuplexStream = imports.sandbox.compileTrustedFunction('streams/tcp-duplex-stream.js')(imports)
  imports.streams.TCPDuplexSourceStream = imports.sandbox.compileTrustedFunction('streams/tcp-duplex-source-stream.js')(imports)

  // run timeouts due
  // run callbacks in queue
  // if nothing left to do, break.
  // poll disk
  // poll network
  function tick () {
    if (queue.length === 0) throw Error("Queue complete.");
    // Dequeue
    var task = queue.shift();
    // Run it
    task[0].apply(null, task.slice(1))
    return true
  }

  return function run (App) {
    var app = App(imports);
    // event loop
    try {
      while (tick()) {}
    } catch (e) {
      if (e.message !== "Queue complete.") imports.log.log(e);
    }
  }
}
